from django.urls import path
from .views import TodoListView, TodoDetailView, TodoCreateView


urlpatterns = [
    path("create/", TodoCreateView.as_view(), name="todo_list_create"),
    path("", TodoListView.as_view(), name="todo_list_list"),
    path("<int:pk>/", TodoDetailView.as_view(), name="todo_list_detail"),
]
